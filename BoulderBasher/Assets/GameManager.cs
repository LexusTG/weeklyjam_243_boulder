using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("The Boulder")]
    public Rigidbody2D boulder;
    private Vector2 boulderOriginalPos;

    [Header("Main UI")]
    public Button simulateButton;
    public TextMeshProUGUI destructionCounterTextField;
    private float destructionCounter;
    private float destructionIncPerObject;
    public GameObject gameOverPopUp;
    public Image gamepverFade;
    public GameObject gameOverPopUpBack;
    public TextMeshProUGUI finalDesctructionTextField;

    //This is now the target to clear the level. It is hard coded for now but we will need to be able to set this PER level we make
    private float destructionCounterTarget = 65f;

    private DestructableObject[] allDestructableObjects;

    //Statics
    public static bool editMode = true;


    public static event Action<bool> inEditMode = delegate {};

    void Start()
    {
        destructionCounterTextField.text = "0%";
        Physics2D.IgnoreLayerCollision(6 , 3);

        boulderOriginalPos = boulder.position;
        simulateButton.onClick.AddListener(Simulate);

        //this finds all objects in the scene which are destructable
        allDestructableObjects = FindObjectsOfType<DestructableObject>();
        DestructableObject.objectDestroyed += IncreaseDestruction;
        SceneManager.sceneUnloaded += SceneUnloaded;

        //this calculates the % of destruction 1 object will add to your total
        destructionIncPerObject = 100f / allDestructableObjects.Length;
    }
    
    void SceneUnloaded(Scene scene)
    {
        simulateButton.onClick.RemoveAllListeners();
        DestructableObject.objectDestroyed -= IncreaseDestruction;
    }

    private void IncreaseDestruction()
    {
        destructionCounter += destructionIncPerObject;
        destructionCounterTextField.text = destructionCounter.ToString("F2") + "%";

        //Check if we met our target then we are don-e!
        if(destructionCounter >= destructionCounterTarget)
        {
           LevelCleared();
        }
    }

    public void LevelCleared()
    {
        //Here we need to animate a level cleared popup.. play a cool sound.. pop some firewords what ever
        gameOverPopUp.SetActive(true);
        gamepverFade.DOFade(0.8f, 1f).SetEase(Ease.OutBack);
        gameOverPopUpBack.transform.DOPunchScale(new Vector3(0.2f,0.2f,0.2f), 0.3f, 5, 1).SetRelative(true).SetEase(Ease.OutBack);

        finalDesctructionTextField.text = destructionCounter.ToString("F2") + "%";
    }

    public void Simulate()
    {
        inEditMode(false);

        Physics2D.IgnoreLayerCollision(6 , 3, false);

        //Activate the boulder
        boulder.simulated = true;

        //lock all the Movable Obstacles
        SetEditMode(false);

        //Transform the simulate button to a stop button
        simulateButton.GetComponentInChildren<TextMeshProUGUI>().text = "Stop Simulation";
        simulateButton.onClick.RemoveAllListeners();
        simulateButton.onClick.AddListener(EndSimulation);
    }

    public void EndSimulation()
    {
        inEditMode(true);

        Physics2D.IgnoreLayerCollision(6 , 3, true);

        //set the boulder on its original position
        boulder.transform.position = boulderOriginalPos;
        //Deactivate the boulder
        boulder.simulated = false;
        boulder.velocity = Vector3.zero;
        boulder.angularVelocity = 0;

        SetEditMode(true);

        //Transform the simulate button to the Go button
        simulateButton.GetComponentInChildren<TextMeshProUGUI>().text = "Go Go Go!!";
        simulateButton.onClick.RemoveAllListeners();
        simulateButton.onClick.AddListener(Simulate);
    }

    private void SetEditMode(bool state)
    {
        editMode = state;
    }

    public void BackToLevelSelect()
    {
        SceneManager.LoadScene("LevelSelect");
    }

}
