using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class DestructableObject : MonoBehaviour
{
    private bool destroyed = false;
    public static event Action objectDestroyed = delegate{};

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player" && !destroyed)
        {
            destroyed = true;

            objectDestroyed();

            GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            GetComponent<Rigidbody2D>().AddForce(new Vector2(UnityEngine.Random.Range(25,50), UnityEngine.Random.Range(25,50)), ForceMode2D.Impulse);

            if(GetComponent<Animator>() != null)
                GetComponent<Animator>().enabled = false;
        }
    }
}
