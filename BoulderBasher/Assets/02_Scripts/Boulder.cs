using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour
{
    public float boostMagnitude = 500;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Boost")
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D>();
            rb.AddForce(other.transform.rotation * Vector3.right * boostMagnitude, ForceMode2D.Impulse);
        }
    }
}
