using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LevelSelectButton : MonoBehaviour
{
    [Header("Scene to load")]
    public string sceneNameToLoad;

    public static event Action<string> loadLevel = delegate {};

    public void LoadSelectedScene()
    {
        if(sceneNameToLoad != null)
        {
            loadLevel(sceneNameToLoad);
        }
    }
}
