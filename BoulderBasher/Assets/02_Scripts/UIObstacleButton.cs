using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIObstacleButton : MonoBehaviour
{
    [Header("Obstacle to Spawn")]
    public GameObject obstacleToSpawn;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(SpawnObstacle);
    }

    void Destroy()
    {
        GetComponent<Button>().onClick.RemoveAllListeners();
    }

    public void SpawnObstacle()
    {
        if(GameManager.editMode)
        {
            GameObject spawnedObject = Instantiate(obstacleToSpawn, new Vector3(0,0,0), obstacleToSpawn.transform.rotation);
            spawnedObject.transform.DOPunchScale(new Vector3(1,1,0), 0.2f, 10, 1).SetEase(Ease.OutElastic).SetRelative(true); 
        }   
    }
}
