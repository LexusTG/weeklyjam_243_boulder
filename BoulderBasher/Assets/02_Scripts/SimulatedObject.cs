using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulatedObject : MonoBehaviour
{
    private Vector2 originalPos;

    // Start is called before the first frame update
    void Start()
    {
        originalPos = GetComponent<Rigidbody2D>().position;
        GameManager.inEditMode += SetSimulated;
    }

    void OnDestroy()
    {
        GameManager.inEditMode -= SetSimulated;
    }

    private void SetSimulated(bool state)
    {
        if(state)
        {
            transform.position = originalPos;
            transform.rotation = Quaternion.identity;

            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        else{
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
