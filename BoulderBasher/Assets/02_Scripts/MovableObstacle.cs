using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MovableObstacle : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;

    public GameObject rotationHandle;
    public GameObject binButton;

    void Awake()
    {
        GameManager.inEditMode += SHowHandle;
        SceneManager.sceneUnloaded += SceneUnloaded;
    }

    void SceneUnloaded(Scene scene)
    {
        GameManager.inEditMode -= SHowHandle;
    }

    private void SHowHandle(bool state)
    {
        rotationHandle.SetActive(state);
        binButton.SetActive(state);
    }

    void OnMouseDown()
    {
        //If we are in edit mode we are allowed to select the movable objects
        if(GameManager.editMode)
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }
 
    void OnMouseDrag()
    {
        if(GameManager.editMode)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
        }
    }

    public void DeleteObject()
    {
        GameManager.inEditMode -= SHowHandle;
        Destroy(gameObject);
    }
}
