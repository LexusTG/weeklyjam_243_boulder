using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class Caveman : MonoBehaviour
{
    [HideInInspector] public bool squashed;
    private Vector2 originalPos;

    public static event Action cavemenSquashed = delegate {};

    void Start()
    {
        originalPos = transform.position;
        GameManager.inEditMode += SetSimulated;
    }

    void OnDestroy()
    {
        GameManager.inEditMode -= SetSimulated;
    }

    private void SetSimulated(bool state)
    {
        if(state && !squashed)
        {
            transform.position = originalPos;
            transform.rotation = Quaternion.identity;
            GetComponentInChildren<Rigidbody2D>().velocity = Vector2.zero;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player" && !squashed)
        {
            if(other.gameObject.transform.position.y > transform.position.y)
            {
                squashed = true;
                Debug.Log("I AM SQUASHED");

                transform.DOScaleY(0.2f, 0.2f).SetEase(Ease.OutBounce); 

                cavemenSquashed();
            }
            
        }
    }
}
