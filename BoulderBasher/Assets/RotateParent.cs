using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateParent : MonoBehaviour
{
    private Vector3 mouse_pos;
    private Vector3 offset;
    private Vector3 screenPoint;
    private float angle;
    private Vector3 object_pos;
 
    void OnMouseDown()
    {
        //Debug.Log("Handle clicked");

        if(GameManager.editMode)
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        //Debug.Log("We are dragint he parent");

        if(GameManager.editMode)
        {
                mouse_pos = Input.mousePosition;
                mouse_pos.z = 0f; //The distance between the camera and object

                object_pos = Camera.main.WorldToScreenPoint(transform.parent.transform.position);

                mouse_pos.x = mouse_pos.x - object_pos.x;
                mouse_pos.y = mouse_pos.y - object_pos.y;

                angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;

                transform.parent.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }
}
