using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteParent : MonoBehaviour
{
    void OnMouseDown()
    {
        GetComponentInParent<MovableObstacle>().DeleteObject();
    }
}
