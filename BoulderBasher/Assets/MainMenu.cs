using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    public GameObject Logo;
    public GameObject tutorial;
    public Image overlay;
    private string levelToLoad;

    // Start is called before the first frame update
    void Awake()
    {
        LevelSelectButton.loadLevel += LoadLevel;
    }

    void Destroy()
    {
        LevelSelectButton.loadLevel -= LoadLevel;
    }

    void LoadLevel(string level)
    {
        levelToLoad = level;
        overlay.DOFade(1, 0.5f).OnComplete(SwitchScene);
    }

    void SwitchScene()
    {
        SceneManager.LoadScene(levelToLoad);
    }   

    void OnEnable()
    {
        overlay.DOFade(0, 0.3f);
        Logo.transform.DOPunchScale(new Vector3(0.2f,0.2f,0.2f), 0.2f, 5, 1).SetRelative(true).SetEase(Ease.OutBack).SetDelay(0.2f);
        tutorial.transform.DOPunchScale(new Vector3(0.2f,0.2f,0.2f), 0.2f, 5, 1).SetRelative(true).SetEase(Ease.OutBack).SetDelay(0.3f);
    }
}
